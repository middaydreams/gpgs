﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class OhMyScript : MonoBehaviour {

	// Use this for initialization
	void Start () {

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        SignIn();
	}

    void SignIn()
    {
        UIScript.Instance.AddToLog("Signing In...");
        Social.localUser.Authenticate( success =>
        {
            if (success)
                UIScript.Instance.AddToLog("Sign In successfull");
            else UIScript.Instance.AddToLog("Sign In Failed!");

            UIScript.Instance.AddToLog("User authed = " + Social.localUser.authenticated.ToString());
        });
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
