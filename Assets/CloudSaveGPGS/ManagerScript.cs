﻿using UnityEngine;

public class ManagerScript : MonoBehaviour {

	public static ManagerScript Instance { get; private set; }
	public static int Counter { get; private set; }

	void Awake ()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start ()
	{
		//UIScript.Instance.UpdateHighscoreText ();
	}

	public void IncrementCounter ()
	{
		Counter++;
		//UIScript.Instance.UpdatePointsText ();
	}

	public void RestartGame ()
	{
		PlayGamesScript.AddScoreToLeaderboard (GPGSIds.leaderboard_global_leaderboard, Counter);

		if (Counter > CloudVariables.Highscore) {
			CloudVariables.Highscore = Counter;
			//PlayGamesScript.Instance.SaveData ();
			PlayGamesController.Instance.SaveData ();
			UIScript.Instance.UpdateHighscoreText ();
		}

		Counter = 0;
		//UIScript.Instance.UpdatePointsText ();
	}

}