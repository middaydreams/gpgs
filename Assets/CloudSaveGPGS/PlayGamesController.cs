﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using System;
using System.Text;

public class PlayGamesController : MonoBehaviour {

	// Use this for initialization
	public static PlayGamesController Instance;

	void Awake ()
	{
		Instance = this;
	}

	void Start ()
	{
		PlayGamesClientConfiguration configuration = new PlayGamesClientConfiguration.Builder ().EnableSavedGames ().Build ();
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.InitializeInstance (configuration);
		PlayGamesPlatform.Activate ();
		SignIn ();
	}

	void SignIn ()
	{
		UIScript.Instance.AddToLog ("Signing In...");
		Social.localUser.Authenticate ((status, message) => {
			if (status == true) {
				UIScript.Instance.AddToLog ("Sign In Done! " + message);
				LoadData ();
			} else {
				UIScript.Instance.AddToLog ("Sign In Failed! " + message);
			}
		});
	}

	#region Save Data
	public void SaveData ()
	{
		Log ("Saving Data...");
		if (Social.localUser.authenticated) {
			Log ("User Authenticated! Opening Saved Game!");
			((PlayGamesPlatform)Social.Active).SavedGame.OpenWithManualConflictResolution (
				"LevelData", DataSource.ReadCacheOrNetwork, true, ConflictResolve, OnSavedGameOpenForSave
			);
		} else {
			Log ("User not Authenticated! No Saving!");
		}
	}

	private void OnSavedGameOpenForSave (SavedGameRequestStatus status, ISavedGameMetadata metadata)
	{
		Log ("Saved Game Open for Save Status = " + status.ToString ());
		if (status == SavedGameRequestStatus.Success) {
			Log ("Save Score = " + CloudVariables.Highscore);
			byte [] dataToSave = Encoding.ASCII.GetBytes (CloudVariables.Highscore.ToString ());
			SavedGameMetadataUpdate MetaUpdate = new SavedGameMetadataUpdate.Builder ().WithUpdatedDescription (DateTime.UtcNow.ToString ()).Build ();
			((PlayGamesPlatform)Social.Active).SavedGame.CommitUpdate (metadata, MetaUpdate, dataToSave, OnSaveDone);
		}
	}

	private void OnSaveDone (SavedGameRequestStatus status, ISavedGameMetadata metadata)
	{
		Log ("Save Binary Data Status = " + status.ToString ());
		Log ("");
	}

	private void ConflictResolve (IConflictResolver resolver, ISavedGameMetadata original, byte [] originalData, ISavedGameMetadata unmerged, byte [] unmergedData)
	{
		if (originalData == null && unmergedData == null) {
			Log ("Both original & unmerged is null");
			Log ("Choosed Original! Lets See!");
			resolver.ChooseMetadata (original);
		} else if (originalData == null) {
			Log ("Unmerged is Okay! Choosing that!");
			resolver.ChooseMetadata (unmerged);
		} else if (unmergedData == null) {
			Log ("Original is Okay! Choosing that!");
			resolver.ChooseMetadata (original);
		} else {
			Log ("Both original & unmerged is Okay! Comparing!");
			int originalScore = int.Parse (Encoding.ASCII.GetString (originalData));
			int unmergedScore = int.Parse (Encoding.ASCII.GetString (unmergedData));
			if (originalScore > unmergedScore) {
				Log ("Original is greater! choosing that!");
				resolver.ChooseMetadata (original);
			} else if (unmergedScore > originalScore) {
				Log ("Unmerged is greater! choosing that!");
				resolver.ChooseMetadata (unmerged);
			} else {
				Log ("Original & Unmerged is Equal! Choosing Original!");
				resolver.ChooseMetadata (original);
			}
		}
		Log ("Manual Conflict Resolve Done!");
	}
	#endregion //Save data

	#region Load Data
	public void LoadData ()
	{
		Log ("Loading Data ...");
		if (Social.localUser.authenticated) {
			Log ("User Authenticated! Opening Saved Game!");
			((PlayGamesPlatform)Social.Active).SavedGame.OpenWithManualConflictResolution ("LevelData", DataSource.ReadCacheOrNetwork, true, ConflictResolve, OnSavedGameOpenedForLoad);
		} else {
			Log ("User not Authenticated! No Loading!");
		}
	}

	private void OnSavedGameOpenedForLoad (SavedGameRequestStatus status, ISavedGameMetadata metadata)
	{
		Log ("Saved Game Opened For Load Status = " + status.ToString ());
		if (status == SavedGameRequestStatus.Success) {
			((PlayGamesPlatform)Social.Active).SavedGame.ReadBinaryData (metadata, OnDataLoad);
		}
	}

	private void OnDataLoad (SavedGameRequestStatus status, byte [] BinaryData)
	{
		Log ("Read Binary Data status = " + status.ToString ());
		if (status == SavedGameRequestStatus.Success) {
			CloudVariables.Highscore = int.Parse (Encoding.ASCII.GetString (BinaryData));
			Log ("Loaded score = " + CloudVariables.Highscore);
		}
	}
	#endregion //Load Data

	#region utilitis
	void Log (string str)
	{
		UIScript.Instance.AddToLog (str);
	}
	#endregion //utilities
}
