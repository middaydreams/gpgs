﻿using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {

	public static UIScript Instance { get; private set; }

	// Use this for initialization
	void Awake ()
	{
		Instance = this;
	}

	void Update ()
	{
		pointsTxt.text = ManagerScript.Counter.ToString ();
		highscoreTxt.text = CloudVariables.Highscore.ToString ();
	}

	[SerializeField]
	private Text pointsTxt;
	[SerializeField]
	private Text highscoreTxt;

	public Text LongLogText;

	public void GetPoint ()
	{
		//AddToLog ("Increment Score!");
		ManagerScript.Instance.IncrementCounter ();
	}

	public void Restart ()
	{
		//PlayGamesScript.Instance.SaveData ();
		//PlayGamesController.Instance.SaveData ();
		ManagerScript.Instance.RestartGame ();
		LongLogText.text += "Restarted\n";
	}

	/*public void Increment ()
	{
		PlayGamesScript.IncrementAchievement (GPGSIds.achievement_incremental_achievement, 5);
	}*/

	public void Unlock ()
	{
		PlayGamesScript.UnlockAchievement (GPGSIds.achievement_master_of_constellation);
	}

	public void ShowAchievements ()
	{
		AddToLog ("Showing Achievements!");
		PlayGamesScript.ShowAchievementsUI ();
	}

	public void ShowLeaderboards ()
	{
		AddToLog ("Showing LeaderboardS!");
		PlayGamesScript.ShowLeaderboardsUI ();
	}

	public void UpdatePointsText ()
	{
		//UIScript.Instance.LongLogText.text += "Update score!\n";
		pointsTxt.text = ManagerScript.Counter.ToString ();
	}

	public void UpdateHighscoreText ()
	{
		//LongLogText.text += "Update High Score!\n";
		highscoreTxt.text = CloudVariables.Highscore.ToString ();
	}

	public void Quiit ()
	{
		//PlayGamesScript.Instance.SaveData ();
		//PlayGamesController.Instance.SaveData ();
		Application.Quit ();
	}

	public void AddToLog (string log)
	{
		LongLogText.text += log + "\n";
	}
	public void ClearLogs ()
	{
		LongLogText.text = "";
	}
}